package com.hcloud.system.authority.entity;

import com.hcloud.common.crud.entity.BaseTreeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "h_system_authority",
        indexes = {@Index(name = "idx_name", columnList = "name")
                , @Index(name = "idx_parnetId", columnList = "parentId")}

)
public class AuthorityEntity extends BaseTreeEntity {
    @NotBlank(message = "权限/菜单名字不能为空")
    private String name;
    @NotNull(message = "排序号不能为空")
    private Integer orderNum;
    private String menuHref;
    private String menuUrl;
    private String menuIcon;
    private String authority;
    @NotNull(message = "权限/菜单类型不可为空")
    private Integer isMenu;

}
