package com.hcloud.system.role.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.hcloud.common.crud.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "h_system_role")
public class RoleEntity extends BaseEntity {
    private String name;
    private String comment;

    @Transient
    private String nameLike;

}
