package com.hcloud.system.api;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

/**
 * 当某一个web项目引入此依赖时，自动配置对应的内容
 * 引入该包下对应的feign
 *
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Configuration
@ConditionalOnWebApplication
@EnableFeignClients({"com.hcloud.system.api.feign"})
public class SystemAutoConfiguration {

}
