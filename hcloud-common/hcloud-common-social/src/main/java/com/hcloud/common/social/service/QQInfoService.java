package com.hcloud.common.social.service;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public interface QQInfoService {
    String getOpenId(String code);
}
